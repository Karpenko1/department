﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Department.Models
{
    public interface IDepartmentRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetEmployeesList(); // получение всех объектов
        //DbSet<T> GetEmployeesList();
        T GetEmployee(int id); // получение одного объекта по id
        void CreateEmployee(T item); // создание объекта
        void UpdateEmployee(T item);
        void DeleteEmployee(int id); // удаление объекта по id
        void Save();  // сохранение изменений
    }
}
