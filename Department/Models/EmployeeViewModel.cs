﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Department.Models
{
    public class EmployeeViewModel : Employee
    {
        public IEnumerable<Employee> Employees { get; set; }
    }
}