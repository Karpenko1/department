﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Department.Models
{
    public class DepartmentRepository : IDepartmentRepository<Employee>
    {
        private DepartmentContext db;

        public DepartmentRepository()
        {
            this.db = new DepartmentContext();
        }

        public IEnumerable<Employee> GetEmployeesList()
        //public DbSet<Employee> GetEmployeeList()
        {
            return db.Employees;
        }

        public Employee GetEmployee(int id)
        {
            return db.Employees.Find(id);
        }

        public void CreateEmployee(Employee employee)
        {
            db.Employees.Add(employee);
        }

        public void UpdateEmployee(Employee employee)
        {
            db.Entry(employee).State = EntityState.Modified;
        }
        public void DeleteEmployee(int id)
        {
            Employee employee = db.Employees.Find(id);
            if (employee != null)
                db.Employees.Remove(employee);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}