﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Department.Models
{
    public class DepartmentContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
    }
}