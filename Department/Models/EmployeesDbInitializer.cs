﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Department.Models
{
    public class EmployeesDbInitializer : DropCreateDatabaseAlways<DepartmentContext>
    {
        protected override void Seed(DepartmentContext db)
        {
            db.Employees.Add(new Employee { Name = "Lee Vitaly", Department = "Cherkassy", Position = "Middle ASP.Net Developer", ChiefId = null });
            db.Employees.Add(new Employee { Name = "Andreeva Marina", Department = "Obuhov", Position = "Junior ASP.Net Developer", ChiefId = 1 });
            db.Employees.Add(new Employee { Name = "Karpenko Oleg", Department = "Zhitomir", Position = "None", ChiefId = 1 });
            base.Seed(db);
        }
    }     
}