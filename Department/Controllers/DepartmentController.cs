﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Department.Models;
using System.Net;

namespace Department.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly IDepartmentRepository<Employee> db;

        public DepartmentController()
        {
            this.db = new DepartmentRepository();
        }

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult EmployeesTable()
        {
            IEnumerable<Employee> EmployeesList = db.GetEmployeesList();
            return PartialView(EmployeesList);
        }


        [HttpGet]
        public ActionResult CreateEmployee()
        {
            EmployeeViewModel EVM = new EmployeeViewModel();
            EVM.Employees = db.GetEmployeesList();
            return PartialView(EVM);
        }


        [HttpPost]
        public ActionResult CreateEmployee([Bind(Include = "Id,Name,Position,Department,ChiefId")]Employee employee)
        {
            if (ModelState.IsValid)
            { 
                db.CreateEmployee(employee);
                db.Save();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            EmployeeViewModel EVM = new EmployeeViewModel
            {
                Id = employee.Id,
                Name = employee.Name,
                Department = employee.Department,
                Position = employee.Position,
                ChiefId = employee.ChiefId,
                Chief = employee.Chief,
                Employees = db.GetEmployeesList()
            };
            return PartialView(EVM);
        }
         

        [HttpGet]
        public ActionResult EditEmployee(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }        
            var employee = db.GetEmployee(id.GetValueOrDefault());
            if (employee == null)
            {
                return HttpNotFound();
            }
            var employees = db.GetEmployeesList().Where(e => e.Id != id).ToList();
            EmployeeViewModel EVM = new EmployeeViewModel
            {
                Id = employee.Id,
                Name = employee.Name,
                Department = employee.Department,
                Position = employee.Position,
                ChiefId = employee.ChiefId,
                Chief = employee.Chief,
                Employees = employees
            };
            return PartialView(EVM);
        }


        [HttpPost]
        public ActionResult EditEmployee([Bind(Include = "Id,Name,Position,Department,ChiefId")]Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.UpdateEmployee(employee);
                db.Save();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            employee.Chief = db.GetEmployee(employee.ChiefId.GetValueOrDefault());
            EmployeeViewModel EVM = new EmployeeViewModel
            {
                Id = employee.Id,
                Name = employee.Name,
                Department = employee.Department,
                Position = employee.Position,
                ChiefId = employee.ChiefId,
                Chief = employee.Chief,
                Employees = db.GetEmployeesList()
            };
            return PartialView(EVM);
        }


        [HttpGet]
        public ActionResult DeleteEmployee(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var employee = db.GetEmployee(id.GetValueOrDefault());
            if (employee == null)
            {
                return HttpNotFound();
            }
            return PartialView(employee);
        }


        [HttpPost]
        [ActionName("DeleteEmployee")]
        public ActionResult ConfirmDelete(int id)
        {
            var employees = db.GetEmployeesList();
            foreach (var e in employees)
            {
                if (id == e.ChiefId)
                {
                    e.ChiefId = null;
                }    
            }
            db.DeleteEmployee(id);
            db.Save();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}